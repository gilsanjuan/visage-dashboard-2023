export const calendarOldColors = [
  { text: 'debe', color: 'bg-[#c54242]' },
  { text: 'agendada', color: 'bg-[#8f908f]' },
  { text: 'notificada', color: 'bg-[#5178a0]' },
  { text: 'cancelada', color: 'bg-[#c58442]' },
  { text: 'confirmada', color: 'bg-[#b9ad1e]' },
  { text: 'pendiente', color: 'bg-[#5191c9]' },
  { text: 'tomada', color: 'bg-[#3c7928]' },
  { text: 're-agendada', color: 'bg-[#c54291]' },
];

export const calendarPropColors = [
  { text: 'agendada', color: 'bg-green-300', textColor: 'bg-green-900' },
  { text: 're-agendada', color: 'bg-orange-200', textColor: 'bg-orange-800' },
  { text: 'notificada', color: 'bg-sky-200', textColor: 'bg-sky-800' },
  { text: 'pendiente', color: 'bg-yellow-200', textColor: 'bg-yellow-800' },
  { text: 'confirmada', color: 'bg-green-600', textColor: 'bg-green-100' },
  { text: 'tomada', color: 'bg-violet-500', textColor: 'bg-violet-100' },
  { text: 'debe', color: 'bg-rose-500', textColor: 'bg-rose-100' },
  { text: 'cancelada', color: 'bg-gray-500', textColor: 'bg-gray-200' },
];

export const calendarColors = calendarPropColors;
