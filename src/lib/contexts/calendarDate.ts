import { writable, type Writable } from 'svelte/store';
import { getContext, setContext } from 'svelte';
// import dayjs, { Dayjs } from 'dayjs';

type DatePickerObj = {
  activeView: string;
  activeViewDirection: number;
  day: number;
  end: Date;
  enlargeDay: boolean;
  hasChosen: boolean;
  month: number;
  open: boolean;
  selected: Date;
  shouldEnlargeDay: boolean;
  start: Date;
  startOfWeekIndex: number;
  year: number;
};

type Context = Writable<DatePickerObj>;
const currentDate: Date = new Date();

export function setDate() {
  const datePickerObj = writable<DatePickerObj>({
    activeView: 'days',
    activeViewDirection: 1,
    day: currentDate.getDay(),
    end: currentDate,
    enlargeDay: false,
    hasChosen: true,
    month: currentDate.getMonth(),
    open: false,
    selected: currentDate,
    shouldEnlargeDay: false,
    start: currentDate,
    startOfWeekIndex: 0,
    year: currentDate.getFullYear(),
  });
  setContext('currentDate', datePickerObj);
}

export function getDate() {
  return getContext<Context>('currentDate');
}
