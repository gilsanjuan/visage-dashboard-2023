import { writable } from 'svelte/store';
import { createClient } from '@supabase/supabase-js';
import {
  PUBLIC_SUPABASE_URL,
  PUBLIC_SUPABASE_ANON_KEY,
} from '$env/static/public';

export const supabase = createClient(
  PUBLIC_SUPABASE_URL,
  PUBLIC_SUPABASE_ANON_KEY,
);

export const currentUser = writable((await supabase.auth.getUser()).data.user);
supabase.auth.onAuthStateChange(async () => {
  currentUser.set((await supabase.auth.getUser()).data.user);
});
