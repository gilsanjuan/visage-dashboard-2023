import dayjs from 'dayjs'

dayjs.locale('de') // use locale globally

export const dateAgo = (date) => {
  if (!date) return '-'

  return dayjs(date).fromNow()
}


export const dateFormat = (date) => {
  if (!date) return '-'
 
  return dayjs(date).format('MMMM DD YYYY')
}

export const daysOfWeek = (date) => {
  if (!date) return '-'

  if( date.length > 0 ) {
    const result = date.map( day => {
      const dayName = dayjs().day(day).format('dddd')
      return dayName.charAt(0).toUpperCase() + dayName.slice(1)
    })

    return result.join(', ')
  }
}

