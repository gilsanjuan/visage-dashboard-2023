function isFloat(n) {
  return n === +n && n !== (n|0);
}

function getFormattedCurrency(num) {
  if (!isFloat) return 0

  num = num !== 0 ? num : num.toFixed(2)
  const cents = (num - Math.floor(num)).toFixed(2);
  return `$${Math.floor(num).toLocaleString() + '.' + cents.split('.')[1] } MXN`;
}

export default function(amount) {
  return getFormattedCurrency(amount)
}