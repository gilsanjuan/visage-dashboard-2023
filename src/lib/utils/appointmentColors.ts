
export const statusList = {
    Agendada: '',
    Notificada: 'blue',
    Confirmada: 'yellow',
    Cancelada: 'red',
    Reagendada: 'pink',
    Pendiente: 'orange',
    Tomada: 'green',
}

export default (status) => {
    return statusList[status]
}