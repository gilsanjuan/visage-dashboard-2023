export const calendarBaseOptions = {
  title: null,
  locale: 'es-MX',
  allDaySlot: false,
  slotMinTime: '09:00:00',
  slotMaxTime: '20:00:00',
  slotDuration: '00:15:00',
  view: 'resourceTimeGridDay',
  headerToolbar: {
    start: '',
    center: '',
    end: 'resourceTimeGridDay,resourceTimeGridWeek, listWeek',
  },
  buttonText: function (texts) {
    texts.listWeek = 'Lista';
    texts.dayGridMonth = 'Mes';
    texts.timeGridWeek = 'Semana';
    texts.resourceTimeGridDay = 'Día';
    texts.resourceTimeGridWeek = 'Semana';
    return texts;
  },
  views: {
    resourceTimeGridDay: { pointer: true },
  },
  dayMaxEvents: true,
  nowIndicator: true,
  selectable: true,
};

export const servicesToHTML = (services, callback) => {
  if (!services?.length) return '';

  let htmlInfo = '<div class="flex space-x-2">';
  services.forEach(service => {
    htmlInfo += `<span data-id="${service.id}"><span class="font-medium"> ${
      service?.name
    }</span>: ${service?.isPayed ? 'Debe' : 'Pagada'}</span>`;
  });

  htmlInfo += '</div>';

  return htmlInfo;
};

function _pad(num) {
  let norm = Math.floor(Math.abs(num));
  return (norm < 10 ? '0' : '') + norm;
}

export function createEvents() {
  let days = [];
  for (let i = 0; i < 7; ++i) {
    let day = new Date();
    let diff = i - day.getDay();
    day.setDate(day.getDate() + diff);
    days[i] =
      day.getFullYear() +
      '-' +
      _pad(day.getMonth() + 1) +
      '-' +
      _pad(day.getDate());
  }

  return [
    {
      id: 1,
      resourceId: 1,
      start: days[0] + ' 09:00',
      end: days[0] + ' 09:45',
      title: 'Agendada',
      // title: 'Thecalendar can events',
      color: '#bbf7d0',
      textColor: '#14532d',
      extendedProps: {
        status: 'agendada',
        folio: 'R-6684',
        firstName: 'Grisel Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
    {
      id: 2,
      start: days[0] + ' 10:00',
      end: days[0] + ' 11:00',
      resourceId: 1,
      title: 'Re-Agendada',
      color: '#fed7aa',
      textColor: '#9a3412',
      extendedProps: {
        status: 're-agendada',
        folio: 'R-6684',
        firstName: 'Anna Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
    {
      id: 3,
      start: days[0] + ' 11:00',
      end: days[0] + ' 11:15',
      resourceId: 1,
      title: 'Notificada',
      color: '#bae6fd',
      textColor: '#075985',
      extendedProps: {
        status: 'notificada',
        folio: 'R-6684',
        firstName: 'Anna Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
    {
      id: 4,
      start: days[0] + ' 11:30',
      end: days[0] + ' 12:00',
      resourceId: 2,
      title: 'Pendiente',
      color: '#fef9c3',
      textColor: '#a16207',
      extendedProps: {
        status: 'pendiente',
        folio: 'R-6684',
        firstName: 'Anna Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
    {
      id: 40,
      start: days[0] + ' 11:30',
      end: days[0] + ' 12:00',
      resourceId: 2,
      title: 'Tomada',
      color: '#8b5cf6',
      textColor: '#ede9fe',
      extendedProps: {
        status: 'pendiente',
        folio: 'R-6684',
        firstName: 'Anna Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
    {
      id: 5,
      start: days[0] + ' 12:00',
      end: days[0] + ' 12:30',
      resourceId: 2,
      title: 'Confirmada',
      color: '#16a34a',
      textColor: '#dcfce7',
      extendedProps: {
        status: 'confirmada',
        folio: 'R-6684',
        firstName: 'Anna Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
    {
      id: 6,
      start: days[0] + ' 12:30',
      end: days[0] + ' 13:30',
      resourceId: 2,
      title: 'Tomada',
      color: '#8b5cf6',
      textColor: '#ede9fe',
      extendedProps: {
        status: 'tomada',
        folio: 'R-6684',
        firstName: 'Anna Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
    {
      id: 7,
      start: days[0] + ' 13:30',
      end: days[0] + ' 14:30',
      resourceId: 2,
      title: 'Debe',
      color: '#f43f5e',
      textColor: '#ffe4e6',
      extendedProps: {
        status: 'debe',
        folio: 'R-6684',
        firstName: 'Anna Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
    {
      id: 8,
      start: days[0] + ' 14:30',
      end: days[0] + ' 15:30',
      resourceId: 2,
      title: 'cancelada',
      color: '#71717a',
      textColor: '#e5e7eb',
      extendedProps: {
        status: 'cancelada',
        folio: 'R-6684',
        firstName: 'Anna Sophie',
        lastName: 'Castillo Membrila',
        services: [
          {
            id: 1,
            name: 'Piernas',
            isPayed: false,
          },
          {
            id: 2,
            name: 'Piernas',
            isPayed: true,
          },
        ],
      },
    },
  ];
}

const theme = {
  // active: 'ec-active',
  // allDay: 'ec-all-day',
  // bgEvent: 'ec-bg-event',
  // bgEvents: 'ec-bg-events',
  // body: 'ec-body',
  // button: 'ec-button',
  // buttonGroup: 'ec-button-group',
  // calendar: 'ec',
  // compact: 'ec-compact',
  // content: 'ec-content',
  // day: 'ec-day',
  // dayFoot: 'ec-day-foot',
  // dayHead: 'ec-day-head',
  // daySide: 'ec-day-side',
  // days: 'ec-days',
  // draggable: 'ec-draggable',
  // dragging: 'ec-dragging',
  // event: 'ec-event',
  // eventBody: 'ec-event-body',
  // eventTag: 'ec-event-tag',
  // eventTime: 'ec-event-time',
  // eventTitle: 'ec-event-title',
  // events: 'ec-events',
  // extra: 'ec-extra',
  // ghost: 'ec-ghost',
  // handle: 'ec-handle',
  // header: 'ec-header',
  // hiddenScroll: 'ec-hidden-scroll',
  // highlight: 'ec-highlight',
  // icon: 'ec-icon',
  // line: 'ec-line',
  // lines: 'ec-lines',
  // list: 'ec-list',
  // month: 'ec-month',
  // noEvents: 'ec-no-events',
  // nowIndicator: 'ec-now-indicator',
  // otherMonth: 'ec-other-month',
  // pointer: 'ec-pointer',
  // popup: 'ec-popup',
  // preview: 'ec-preview',
  // resizer: 'ec-resizer',
  // resizingX: 'ec-resizing-x',
  // resizingY: 'ec-resizing-y',
  // resource: 'ec-resource',
  // resourceTitle: 'ec-resource-title',
  // selecting: 'ec-selecting',
  // sidebar: 'ec-sidebar',
  // sidebarTitle: 'ec-sidebar-title',
  // time: 'ec-time',
  // title: 'ec-title',
  // today: 'ec-today',
  // toolbar: 'ec-toolbar',
  // uniform: 'ec-uniform',
  // week: 'ec-week',
  // withScroll: 'ec-with-scroll',
};
