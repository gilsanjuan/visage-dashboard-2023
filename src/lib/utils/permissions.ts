export const areCommonElements = (arr1, arr2) => {
  const arr2Set = new Set(arr2);
  return arr1.some(el => arr2Set.has(el));
}

export const areCommonPermissions = (arr1, arr2) => {
  const [shortArr, longArr] = (arr1.length < arr2.length) ? [arr1, arr2] : [arr2, arr1];
  const longArrSet = new Set(longArr);
  return shortArr.some(el => longArrSet.has(el));
};

export const permissions = {
  deletePermission: ['Administrador', 'Recepcionista'],
}

export default {
  ...permissions
}