import type { Actions } from './$types';
import { z } from 'zod';

const loginSchema = z.object({
  email: z
    .string({
      required_error: 'Usuario es requerido',
    })
    .email({ message: 'Escribe una dirección de coreo válida' })
    .min(2, { message: 'Usuario es requerido' })
    .trim(),
  password: z
    .string({ required_error: 'Password es requerido' })
    .min(2, { message: 'Password es requerido' })
    .trim(),
});

/** @type {import('./$types').Actions} */
export const actions: Actions = {
  default: async ({ request }) => {
    const formData = Object.fromEntries(await request.formData());
    // console.log('DATAFORM: ', formData);

    try {
      const loginData = loginSchema.parse(formData);
      // console.log('SUCCESS: ', loginData);
    } catch (err) {
      // console.log(err);
      const { fieldErrors: errors } = err?.flatten();
      return {
        data: formData.email,
        errors,
      };
    }

    // if (!loginData.success) {
    //     const errors = loginData.error.errors.map(error => {
    //         return {
    //             field: error.path[0],
    //             message: error.message,
    //         };
    //     });

    //     return invalid(400, { error: true, errors });
    // }
  },
};
