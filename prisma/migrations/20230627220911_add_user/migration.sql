-- AlterTable
ALTER TABLE "Staff" ADD COLUMN     "phone" TEXT[],
ADD COLUMN     "userId" TEXT;

-- CreateTable
CREATE TABLE "User" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "phone" TEXT[],
    "password" TEXT NOT NULL,
    "provider" TEXT,
    "lastSign" TIMESTAMP(3),

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Staff" ADD CONSTRAINT "Staff_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
