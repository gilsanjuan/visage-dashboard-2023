-- CreateEnum
CREATE TYPE "AppointmentScore" AS ENUM ('Excelente', 'Buena', 'Regular', 'Malo');

-- CreateEnum
CREATE TYPE "AppointmentStatus" AS ENUM ('Agendada', 'Notificada', 'Confirmada', 'Cancelada', 'Reagendada', 'Pendiente', 'Tomada');

-- CreateEnum
CREATE TYPE "CommissionType" AS ENUM ('Servicios', 'Productos');

-- CreateEnum
CREATE TYPE "DiscountType" AS ENUM ('Fijo', 'Porcentaje');

-- CreateEnum
CREATE TYPE "Genre" AS ENUM ('Femenino', 'Masculino', 'Otro');

-- CreateEnum
CREATE TYPE "HairColor" AS ENUM ('Rubio', 'Cafe', 'Cafe_claro', 'Cafe_obscuro', 'Negro');

-- CreateEnum
CREATE TYPE "HairType" AS ENUM ('Fino', 'Medio', 'Grueso');

-- CreateEnum
CREATE TYPE "Medicine" AS ENUM ('Isotreintoin', 'HormonasAnticonceptivos', 'Tiroides', 'Antibiotico', 'Dermatologico', 'Minixodil', 'FuegoHerpes', 'Alergias', 'Cosmetico', 'Embarazo', 'Otro');

-- CreateEnum
CREATE TYPE "OrderStatus" AS ENUM ('Creada', 'Procesando', 'Parcial', 'Pagada');

-- CreateEnum
CREATE TYPE "PaymentMethod" AS ENUM ('Efectivo', 'Tarjeta_debito', 'Tarjeta_credito', 'Tarjeta_credito_3', 'Tarjeta_credito_6', 'Tarjeta_credito_12', 'Transferencia_bancaria', 'Ecommerce', 'Otro');

-- CreateEnum
CREATE TYPE "Role" AS ENUM ('Administrador', 'Enfermera', 'Especialista', 'Recepcionista');

-- CreateEnum
CREATE TYPE "SkinType" AS ENUM ('I', 'II', 'III', 'IV', 'V');

-- CreateTable
CREATE TABLE "Address" (
    "id" TEXT NOT NULL,
    "line1" VARCHAR(255) NOT NULL,
    "line2" VARCHAR(255),
    "city" VARCHAR(50) NOT NULL,
    "state" VARCHAR(50) NOT NULL,
    "area" VARCHAR(100) NOT NULL,
    "latlng" DOUBLE PRECISION[],
    "postalCode" VARCHAR(10) NOT NULL,

    CONSTRAINT "Address_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Clinic" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "phones" TEXT[],
    "email" TEXT,
    "rfc" TEXT,
    "addressId" TEXT,
    "sortOrder" INTEGER NOT NULL DEFAULT 1,
    "isActive" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Clinic_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OpenHours" (
    "id" TEXT NOT NULL,
    "opens" TEXT NOT NULL,
    "closes" TEXT NOT NULL,
    "clinicId" TEXT NOT NULL,
    "daysOfWeek" INTEGER[],

    CONSTRAINT "OpenHours_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Cab" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "clinicId" TEXT,
    "categoryId" TEXT,
    "sortOrder" INTEGER DEFAULT 1,
    "isActive" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Cab_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Staff" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "firstName" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "role" "Role" NOT NULL,
    "clinicId" TEXT,
    "isActive" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Staff_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Commission" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "type" "CommissionType" NOT NULL,
    "percentage" INTEGER NOT NULL,
    "isActive" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Commission_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Category" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "sortOrder" INTEGER NOT NULL DEFAULT 1,
    "isVisible" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Category_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Service" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "icon" TEXT,
    "price" DECIMAL(65,30) NOT NULL,
    "duration" INTEGER NOT NULL,
    "sortOrder" INTEGER NOT NULL DEFAULT 1,
    "categoryId" TEXT,
    "description" TEXT,
    "isActive" BOOLEAN DEFAULT false,
    "genre" "Genre" DEFAULT 'Femenino',

    CONSTRAINT "Service_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ServicePhoto" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "size" INTEGER,
    "type" TEXT NOT NULL,
    "serviceId" TEXT NOT NULL,

    CONSTRAINT "ServicePhoto_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Product" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "price" DECIMAL(65,30) NOT NULL,
    "categoryId" TEXT,
    "description" TEXT,
    "sortOrder" INTEGER NOT NULL DEFAULT 1,
    "isActive" BOOLEAN DEFAULT false,

    CONSTRAINT "Product_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ProductPhoto" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "size" INTEGER,
    "type" TEXT NOT NULL,
    "productId" TEXT NOT NULL,

    CONSTRAINT "ProductPhoto_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ProductStock" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "stock" INTEGER NOT NULL,
    "clinicId" TEXT NOT NULL,
    "productId" TEXT NOT NULL,
    "isActive" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "ProductStock_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Supply" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "price" DECIMAL(65,30) NOT NULL,
    "description" TEXT,
    "isActive" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Supply_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SupplyStock" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "stock" INTEGER NOT NULL,
    "clinicId" TEXT NOT NULL,
    "supplyId" TEXT NOT NULL,
    "isActive" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "SupplyStock_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Package" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "price" DECIMAL(65,30) NOT NULL,
    "originalPrice" DECIMAL(65,30) NOT NULL,
    "discount" INTEGER,
    "isActive" BOOLEAN NOT NULL DEFAULT false,
    "genre" "Genre" NOT NULL DEFAULT 'Femenino',

    CONSTRAINT "Package_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Client" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "folio" TEXT,
    "firstName" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "birthday" DATE,
    "email" TEXT,
    "phone" TEXT,
    "genre" "Genre" NOT NULL,
    "clinicId" TEXT NOT NULL,
    "password" TEXT,
    "note" TEXT,
    "isActive" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Client_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ClientContract" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "clientId" TEXT NOT NULL,
    "contractSignature" TEXT NOT NULL,
    "contractSignatureBase64" TEXT NOT NULL,

    CONSTRAINT "ClientContract_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ClientParent" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "firstName" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "phone" TEXT,
    "clientId" TEXT NOT NULL,

    CONSTRAINT "ClientParent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ClientTraits" (
    "id" TEXT NOT NULL,
    "clientId" TEXT NOT NULL,
    "skinType" "SkinType" NOT NULL,
    "hairType" "HairType" NOT NULL,
    "hairColor" "HairColor" NOT NULL,
    "medicine" "Medicine" NOT NULL,
    "medicineDescription" TEXT,

    CONSTRAINT "ClientTraits_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Appointment" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "note" TEXT,
    "cabId" TEXT NOT NULL,
    "staffId" TEXT,
    "clinicId" TEXT NOT NULL,
    "clientId" TEXT NOT NULL,
    "startDate" TIMESTAMP(3) NOT NULL,
    "duration" INTEGER NOT NULL,
    "status" "AppointmentStatus" NOT NULL,

    CONSTRAINT "Appointment_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AppointmentFeedback" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "ejcm2" TEXT,
    "score" "AppointmentScore" NOT NULL,
    "changes" TEXT,
    "appointmentId" TEXT NOT NULL,

    CONSTRAINT "AppointmentFeedback_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Discount" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "type" "DiscountType" NOT NULL,
    "value" DECIMAL(65,30) NOT NULL,
    "startDate" TIMESTAMP(3) NOT NULL,
    "endDate" DATE,

    CONSTRAINT "Discount_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Voucher" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "name" TEXT,
    "code" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "used" INTEGER NOT NULL DEFAULT 0,
    "startDate" DATE NOT NULL,
    "endDate" DATE,
    "usageLimit" INTEGER,
    "discountValue" DECIMAL(65,30) NOT NULL,
    "minAmountSpent" DECIMAL(65,30),
    "discountValueType" TEXT NOT NULL,

    CONSTRAINT "Voucher_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "BuyXGetY" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "code" TEXT NOT NULL,
    "used" INTEGER DEFAULT 0,
    "value" DECIMAL(65,30),
    "endDate" TIMESTAMP(3),
    "startDate" DATE NOT NULL,
    "usageLimit" INTEGER,
    "quantityToBuy" INTEGER,
    "quantityToGet" INTEGER,
    "minAmountSpent" DECIMAL(65,30),

    CONSTRAINT "BuyXGetY_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Order" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "tax" DECIMAL(65,30),
    "total" DECIMAL(65,30),
    "clientId" TEXT NOT NULL,
    "clinicId" TEXT,
    "subtotal" DECIMAL(65,30),
    "datePayed" TIMESTAMP(3),
    "dateCreated" TIMESTAMP(3),
    "discountName" TEXT,
    "discountAmount" DECIMAL(65,30),
    "orderNote" TEXT,
    "customerNote" TEXT,
    "status" "OrderStatus" NOT NULL,

    CONSTRAINT "Order_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OrderEvent" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "id" TEXT NOT NULL,
    "type" TEXT,
    "staffId" TEXT NOT NULL,
    "orderId" TEXT NOT NULL,

    CONSTRAINT "OrderEvent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OrderLine" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "stock" TEXT,
    "orderId" TEXT,
    "quantity" INTEGER NOT NULL,
    "totalNet" DECIMAL(65,30),
    "productName" TEXT NOT NULL,
    "productType" TEXT,
    "unitPriceNet" DECIMAL(65,30) NOT NULL,
    "discountName" TEXT,
    "discountAmount" DECIMAL(65,30),
    "status" "OrderStatus" NOT NULL,

    CONSTRAINT "OrderLine_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Payment" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "date" DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "token" TEXT,
    "staffId" TEXT,
    "payMethod" "PaymentMethod" NOT NULL,
    "orderLineId" TEXT,
    "productName" TEXT NOT NULL,
    "capturedAmount" DECIMAL(65,30) NOT NULL,
    "chargeStatus" TEXT,
    "extraData" JSONB,

    CONSTRAINT "Payment_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PaymentTransaction" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "fees" DECIMAL(65,30) NOT NULL,
    "token" TEXT NOT NULL,
    "error" TEXT,
    "kind" TEXT NOT NULL,
    "amount" DECIMAL(65,30) NOT NULL,
    "gatewayResponse" TEXT NOT NULL,
    "isSuccess" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "PaymentTransaction_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CommissionOrder" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "total" DECIMAL(65,30) NOT NULL,
    "orderLineId" TEXT NOT NULL,
    "isPayed" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "CommissionOrder_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CommissionPayment" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "date" TIMESTAMP(3),
    "status" BOOLEAN NOT NULL DEFAULT false,
    "paymentId" TEXT NOT NULL,
    "commissionId" TEXT NOT NULL,
    "capturedAmount" DECIMAL(65,30) NOT NULL,

    CONSTRAINT "CommissionPayment_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CashFund" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "date" DATE NOT NULL,
    "total" DECIMAL(65,30) NOT NULL,
    "clinicId" TEXT NOT NULL,

    CONSTRAINT "CashFund_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CashSpending" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "date" DATE NOT NULL,
    "total" DECIMAL(65,30) NOT NULL,
    "clinicId" TEXT NOT NULL,
    "description" TEXT NOT NULL,

    CONSTRAINT "CashSpending_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CashWithdrawal" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "id" TEXT NOT NULL,
    "date" DATE NOT NULL,
    "total" DECIMAL(65,30) NOT NULL,
    "clinicId" TEXT NOT NULL,
    "description" TEXT NOT NULL,

    CONSTRAINT "CashWithdrawal_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_CategoryToVoucher" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_CategoryToDiscount" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_CategorySubcategories" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_ServiceToSupply" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_ServiceToVoucher" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_ProductToVoucher" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_PackageToProduct" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_PackageToService" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_PackageToVoucher" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_AppointmentToService" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_AppointmentFeedbackToService" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_DiscountToProduct" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_DiscountToService" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_ServicesToBuyToServices" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_ServicesToGetToServices" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_OrderToVoucher" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_PaymentToPaymentTransaction" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_CommissionOrderToStaff" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Clinic_addressId_key" ON "Clinic"("addressId");

-- CreateIndex
CREATE UNIQUE INDEX "Staff_email_key" ON "Staff"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Staff_firstName_lastName_key" ON "Staff"("firstName", "lastName");

-- CreateIndex
CREATE UNIQUE INDEX "Service_slug_key" ON "Service"("slug");

-- CreateIndex
CREATE UNIQUE INDEX "Client_folio_key" ON "Client"("folio");

-- CreateIndex
CREATE UNIQUE INDEX "Client_email_key" ON "Client"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Client_firstName_lastName_key" ON "Client"("firstName", "lastName");

-- CreateIndex
CREATE UNIQUE INDEX "ClientContract_clientId_key" ON "ClientContract"("clientId");

-- CreateIndex
CREATE UNIQUE INDEX "ClientParent_clientId_key" ON "ClientParent"("clientId");

-- CreateIndex
CREATE UNIQUE INDEX "ClientTraits_clientId_key" ON "ClientTraits"("clientId");

-- CreateIndex
CREATE UNIQUE INDEX "Voucher_code_key" ON "Voucher"("code");

-- CreateIndex
CREATE UNIQUE INDEX "BuyXGetY_code_key" ON "BuyXGetY"("code");

-- CreateIndex
CREATE UNIQUE INDEX "CommissionOrder_orderLineId_key" ON "CommissionOrder"("orderLineId");

-- CreateIndex
CREATE UNIQUE INDEX "CommissionPayment_paymentId_key" ON "CommissionPayment"("paymentId");

-- CreateIndex
CREATE UNIQUE INDEX "_CategoryToVoucher_AB_unique" ON "_CategoryToVoucher"("A", "B");

-- CreateIndex
CREATE INDEX "_CategoryToVoucher_B_index" ON "_CategoryToVoucher"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_CategoryToDiscount_AB_unique" ON "_CategoryToDiscount"("A", "B");

-- CreateIndex
CREATE INDEX "_CategoryToDiscount_B_index" ON "_CategoryToDiscount"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_CategorySubcategories_AB_unique" ON "_CategorySubcategories"("A", "B");

-- CreateIndex
CREATE INDEX "_CategorySubcategories_B_index" ON "_CategorySubcategories"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_ServiceToSupply_AB_unique" ON "_ServiceToSupply"("A", "B");

-- CreateIndex
CREATE INDEX "_ServiceToSupply_B_index" ON "_ServiceToSupply"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_ServiceToVoucher_AB_unique" ON "_ServiceToVoucher"("A", "B");

-- CreateIndex
CREATE INDEX "_ServiceToVoucher_B_index" ON "_ServiceToVoucher"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_ProductToVoucher_AB_unique" ON "_ProductToVoucher"("A", "B");

-- CreateIndex
CREATE INDEX "_ProductToVoucher_B_index" ON "_ProductToVoucher"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_PackageToProduct_AB_unique" ON "_PackageToProduct"("A", "B");

-- CreateIndex
CREATE INDEX "_PackageToProduct_B_index" ON "_PackageToProduct"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_PackageToService_AB_unique" ON "_PackageToService"("A", "B");

-- CreateIndex
CREATE INDEX "_PackageToService_B_index" ON "_PackageToService"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_PackageToVoucher_AB_unique" ON "_PackageToVoucher"("A", "B");

-- CreateIndex
CREATE INDEX "_PackageToVoucher_B_index" ON "_PackageToVoucher"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_AppointmentToService_AB_unique" ON "_AppointmentToService"("A", "B");

-- CreateIndex
CREATE INDEX "_AppointmentToService_B_index" ON "_AppointmentToService"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_AppointmentFeedbackToService_AB_unique" ON "_AppointmentFeedbackToService"("A", "B");

-- CreateIndex
CREATE INDEX "_AppointmentFeedbackToService_B_index" ON "_AppointmentFeedbackToService"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_DiscountToProduct_AB_unique" ON "_DiscountToProduct"("A", "B");

-- CreateIndex
CREATE INDEX "_DiscountToProduct_B_index" ON "_DiscountToProduct"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_DiscountToService_AB_unique" ON "_DiscountToService"("A", "B");

-- CreateIndex
CREATE INDEX "_DiscountToService_B_index" ON "_DiscountToService"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_ServicesToBuyToServices_AB_unique" ON "_ServicesToBuyToServices"("A", "B");

-- CreateIndex
CREATE INDEX "_ServicesToBuyToServices_B_index" ON "_ServicesToBuyToServices"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_ServicesToGetToServices_AB_unique" ON "_ServicesToGetToServices"("A", "B");

-- CreateIndex
CREATE INDEX "_ServicesToGetToServices_B_index" ON "_ServicesToGetToServices"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_OrderToVoucher_AB_unique" ON "_OrderToVoucher"("A", "B");

-- CreateIndex
CREATE INDEX "_OrderToVoucher_B_index" ON "_OrderToVoucher"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_PaymentToPaymentTransaction_AB_unique" ON "_PaymentToPaymentTransaction"("A", "B");

-- CreateIndex
CREATE INDEX "_PaymentToPaymentTransaction_B_index" ON "_PaymentToPaymentTransaction"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_CommissionOrderToStaff_AB_unique" ON "_CommissionOrderToStaff"("A", "B");

-- CreateIndex
CREATE INDEX "_CommissionOrderToStaff_B_index" ON "_CommissionOrderToStaff"("B");

-- AddForeignKey
ALTER TABLE "Clinic" ADD CONSTRAINT "Clinic_addressId_fkey" FOREIGN KEY ("addressId") REFERENCES "Address"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OpenHours" ADD CONSTRAINT "OpenHours_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Cab" ADD CONSTRAINT "Cab_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Cab" ADD CONSTRAINT "Cab_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Staff" ADD CONSTRAINT "Staff_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Service" ADD CONSTRAINT "Service_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ServicePhoto" ADD CONSTRAINT "ServicePhoto_serviceId_fkey" FOREIGN KEY ("serviceId") REFERENCES "Service"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Product" ADD CONSTRAINT "Product_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ProductPhoto" ADD CONSTRAINT "ProductPhoto_productId_fkey" FOREIGN KEY ("productId") REFERENCES "Product"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ProductStock" ADD CONSTRAINT "ProductStock_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ProductStock" ADD CONSTRAINT "ProductStock_productId_fkey" FOREIGN KEY ("productId") REFERENCES "Product"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SupplyStock" ADD CONSTRAINT "SupplyStock_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SupplyStock" ADD CONSTRAINT "SupplyStock_supplyId_fkey" FOREIGN KEY ("supplyId") REFERENCES "Supply"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Client" ADD CONSTRAINT "Client_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE RESTRICT ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "ClientContract" ADD CONSTRAINT "ClientContract_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ClientParent" ADD CONSTRAINT "ClientParent_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ClientTraits" ADD CONSTRAINT "ClientTraits_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Appointment" ADD CONSTRAINT "Appointment_cabId_fkey" FOREIGN KEY ("cabId") REFERENCES "Cab"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Appointment" ADD CONSTRAINT "Appointment_staffId_fkey" FOREIGN KEY ("staffId") REFERENCES "Staff"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Appointment" ADD CONSTRAINT "Appointment_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Appointment" ADD CONSTRAINT "Appointment_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppointmentFeedback" ADD CONSTRAINT "AppointmentFeedback_appointmentId_fkey" FOREIGN KEY ("appointmentId") REFERENCES "Appointment"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Order" ADD CONSTRAINT "Order_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Order" ADD CONSTRAINT "Order_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderEvent" ADD CONSTRAINT "OrderEvent_staffId_fkey" FOREIGN KEY ("staffId") REFERENCES "Staff"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderEvent" ADD CONSTRAINT "OrderEvent_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "Order"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderLine" ADD CONSTRAINT "OrderLine_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "Order"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Payment" ADD CONSTRAINT "Payment_staffId_fkey" FOREIGN KEY ("staffId") REFERENCES "Staff"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Payment" ADD CONSTRAINT "Payment_orderLineId_fkey" FOREIGN KEY ("orderLineId") REFERENCES "OrderLine"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CommissionOrder" ADD CONSTRAINT "CommissionOrder_orderLineId_fkey" FOREIGN KEY ("orderLineId") REFERENCES "OrderLine"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CommissionPayment" ADD CONSTRAINT "CommissionPayment_paymentId_fkey" FOREIGN KEY ("paymentId") REFERENCES "Payment"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CommissionPayment" ADD CONSTRAINT "CommissionPayment_commissionId_fkey" FOREIGN KEY ("commissionId") REFERENCES "CommissionOrder"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CashFund" ADD CONSTRAINT "CashFund_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CashSpending" ADD CONSTRAINT "CashSpending_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CashWithdrawal" ADD CONSTRAINT "CashWithdrawal_clinicId_fkey" FOREIGN KEY ("clinicId") REFERENCES "Clinic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToVoucher" ADD CONSTRAINT "_CategoryToVoucher_A_fkey" FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToVoucher" ADD CONSTRAINT "_CategoryToVoucher_B_fkey" FOREIGN KEY ("B") REFERENCES "Voucher"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToDiscount" ADD CONSTRAINT "_CategoryToDiscount_A_fkey" FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToDiscount" ADD CONSTRAINT "_CategoryToDiscount_B_fkey" FOREIGN KEY ("B") REFERENCES "Discount"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategorySubcategories" ADD CONSTRAINT "_CategorySubcategories_A_fkey" FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategorySubcategories" ADD CONSTRAINT "_CategorySubcategories_B_fkey" FOREIGN KEY ("B") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ServiceToSupply" ADD CONSTRAINT "_ServiceToSupply_A_fkey" FOREIGN KEY ("A") REFERENCES "Service"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ServiceToSupply" ADD CONSTRAINT "_ServiceToSupply_B_fkey" FOREIGN KEY ("B") REFERENCES "Supply"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ServiceToVoucher" ADD CONSTRAINT "_ServiceToVoucher_A_fkey" FOREIGN KEY ("A") REFERENCES "Service"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ServiceToVoucher" ADD CONSTRAINT "_ServiceToVoucher_B_fkey" FOREIGN KEY ("B") REFERENCES "Voucher"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ProductToVoucher" ADD CONSTRAINT "_ProductToVoucher_A_fkey" FOREIGN KEY ("A") REFERENCES "Product"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ProductToVoucher" ADD CONSTRAINT "_ProductToVoucher_B_fkey" FOREIGN KEY ("B") REFERENCES "Voucher"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PackageToProduct" ADD CONSTRAINT "_PackageToProduct_A_fkey" FOREIGN KEY ("A") REFERENCES "Package"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PackageToProduct" ADD CONSTRAINT "_PackageToProduct_B_fkey" FOREIGN KEY ("B") REFERENCES "Product"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PackageToService" ADD CONSTRAINT "_PackageToService_A_fkey" FOREIGN KEY ("A") REFERENCES "Package"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PackageToService" ADD CONSTRAINT "_PackageToService_B_fkey" FOREIGN KEY ("B") REFERENCES "Service"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PackageToVoucher" ADD CONSTRAINT "_PackageToVoucher_A_fkey" FOREIGN KEY ("A") REFERENCES "Package"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PackageToVoucher" ADD CONSTRAINT "_PackageToVoucher_B_fkey" FOREIGN KEY ("B") REFERENCES "Voucher"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AppointmentToService" ADD CONSTRAINT "_AppointmentToService_A_fkey" FOREIGN KEY ("A") REFERENCES "Appointment"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AppointmentToService" ADD CONSTRAINT "_AppointmentToService_B_fkey" FOREIGN KEY ("B") REFERENCES "Service"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AppointmentFeedbackToService" ADD CONSTRAINT "_AppointmentFeedbackToService_A_fkey" FOREIGN KEY ("A") REFERENCES "AppointmentFeedback"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AppointmentFeedbackToService" ADD CONSTRAINT "_AppointmentFeedbackToService_B_fkey" FOREIGN KEY ("B") REFERENCES "Service"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DiscountToProduct" ADD CONSTRAINT "_DiscountToProduct_A_fkey" FOREIGN KEY ("A") REFERENCES "Discount"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DiscountToProduct" ADD CONSTRAINT "_DiscountToProduct_B_fkey" FOREIGN KEY ("B") REFERENCES "Product"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DiscountToService" ADD CONSTRAINT "_DiscountToService_A_fkey" FOREIGN KEY ("A") REFERENCES "Discount"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DiscountToService" ADD CONSTRAINT "_DiscountToService_B_fkey" FOREIGN KEY ("B") REFERENCES "Service"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ServicesToBuyToServices" ADD CONSTRAINT "_ServicesToBuyToServices_A_fkey" FOREIGN KEY ("A") REFERENCES "BuyXGetY"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ServicesToBuyToServices" ADD CONSTRAINT "_ServicesToBuyToServices_B_fkey" FOREIGN KEY ("B") REFERENCES "Service"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ServicesToGetToServices" ADD CONSTRAINT "_ServicesToGetToServices_A_fkey" FOREIGN KEY ("A") REFERENCES "BuyXGetY"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ServicesToGetToServices" ADD CONSTRAINT "_ServicesToGetToServices_B_fkey" FOREIGN KEY ("B") REFERENCES "Service"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_OrderToVoucher" ADD CONSTRAINT "_OrderToVoucher_A_fkey" FOREIGN KEY ("A") REFERENCES "Order"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_OrderToVoucher" ADD CONSTRAINT "_OrderToVoucher_B_fkey" FOREIGN KEY ("B") REFERENCES "Voucher"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PaymentToPaymentTransaction" ADD CONSTRAINT "_PaymentToPaymentTransaction_A_fkey" FOREIGN KEY ("A") REFERENCES "Payment"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PaymentToPaymentTransaction" ADD CONSTRAINT "_PaymentToPaymentTransaction_B_fkey" FOREIGN KEY ("B") REFERENCES "PaymentTransaction"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CommissionOrderToStaff" ADD CONSTRAINT "_CommissionOrderToStaff_A_fkey" FOREIGN KEY ("A") REFERENCES "CommissionOrder"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CommissionOrderToStaff" ADD CONSTRAINT "_CommissionOrderToStaff_B_fkey" FOREIGN KEY ("B") REFERENCES "Staff"("id") ON DELETE CASCADE ON UPDATE CASCADE;
