module.exports = {
  semi: true,
  plugins: [
    require('prettier-plugin-svelte'),
    require('prettier-plugin-tailwindcss')
  ],
  tabWidth: 2,
  useTabs: false,
  printWidth: 80,
  singleQuote: true,
  proseWrap: 'always',
  arrowParens: 'avoid',
  bracketSpacing: true,
  trailingComma: "all",
  pluginSearchDirs: ["."],
  tailwindConfig: './tailwind.config.js',
  // overrides: [{ "files": "*.svelte", "options": { "parser": "svelte" } }]
}
